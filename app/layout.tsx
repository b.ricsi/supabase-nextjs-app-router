// https://supabase.com/docs/guides/auth/auth-helpers/nextjs#creating-a-supabase-client

// need for cookie based auth instead of local storage
// https://youtu.be/Bh1TOpOcGJQ?t=748

import "@/globals.css";
import type { Metadata } from "next";
import { Inter } from "next/font/google";
import Header from "@/components/header/header";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Create Next App",
  description: "Generated by create next app",
};

export default async function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <Header />
        {children}
      </body>
    </html>
  );
}
