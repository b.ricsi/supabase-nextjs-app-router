import { cookies } from "next/headers";
import { createServerComponentClient } from "@supabase/auth-helpers-nextjs";

import { notFound } from "next/navigation";
import RealtimeRecord from "@/components/records/realtime-record";

export const revalidate = 0;

export default async function ServerDirectFetchPage({
  params: { id },
}: {
  params: { id: string };
}) {
  const cookieData = cookies();

  const supabase = createServerComponentClient({
    cookies: () => cookieData,
  });

  const { data } = await supabase
    .from("tutorial")
    .select()
    .match({ id })
    .single();

  if (!data) notFound();

  const {
    data: { session },
  } = await supabase.auth.getSession();

  return session && <RealtimeRecord recordProp={data} />;
}
