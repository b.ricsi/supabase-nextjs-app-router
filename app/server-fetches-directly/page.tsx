import { cookies } from "next/headers";
import { createServerComponentClient } from "@supabase/auth-helpers-nextjs";

import RealtimeRecords from "@/components/records/realtime-records";
import AddRecordViaServerAction from "@/components/records/add-record-via-server-action";

// this component fetches the current records server-side
// and subscribes to new records client-side
export default async function ServerDirectFetchPage() {
  // https://github.com/vercel/next.js/issues/49373#issuecomment-1662263802
  // https://nextjs.org/docs/messages/dynamic-server-error
  const cookieData = cookies();
  const supabase = createServerComponentClient({
    cookies: () => cookieData,
  });

  const { data } = await supabase
    .from("tutorial")
    .select()
    .match({ is_hidden: false });

  const {
    data: { session },
  } = await supabase.auth.getSession();

  return (
    session && (
      <>
        {/* Server Actions can be used in Server Components only */}
        <AddRecordViaServerAction />

        {/* data can be passed from server components to client components this allows us to fetch the initial records before rendering the page our <RealtimeRecords /> component will then subscribe to new records client-side */}
        <RealtimeRecords recordsProp={data as RecordDB[]} />
      </>
    )
  );
}
