import { createRouteHandlerClient } from "@supabase/auth-helpers-nextjs";
import { cookies } from "next/headers";

import { revalidatePath } from "next/cache";
import { NextResponse } from "next/server";

// https://youtu.be/6MoYy62E4rw?t=373
// Making route-handlers dynamic (bypassing nextjs' default caching) can be via
// - either the same revalidation as doing in server components: export const revalidate = 0
// - or having a route other than GET
// https://nextjs.org/docs/app/building-your-application/data-fetching/fetching-caching-and-revalidating#opting-out-of-data-caching

// export const dynamic = "force-dynamic";

export async function GET() {
  const cookieData = cookies();
  const supabase = createRouteHandlerClient({
    cookies: () => cookieData,
  });

  const { data } = await supabase
    .from("tutorial")
    .select()
    .match({ is_hidden: false });

  return NextResponse.json(data);
}

export async function PUT(request: Request) {
  const requestUrl = new URL(request.url);

  const { id } = await request.json();

  const cookieData = cookies();
  const supabase = createRouteHandlerClient({
    cookies: () => cookieData,
  });

  const { data } = await supabase
    .from("tutorial")
    .update({ is_hidden: true })
    .match({ id });

  revalidatePath(requestUrl.origin);

  return NextResponse.json(data);
}

export async function POST(request: Request) {
  const { title } = await request.json();

  const cookieData = cookies();
  const supabase = createRouteHandlerClient({
    cookies: () => cookieData,
  });

  const { data } = await supabase.from("tutorial").insert({ title }).select();

  return NextResponse.json(data);
}
