"use client";

import { useEffect, useState } from "react";
import { usePathname } from "next/navigation";
import useGetClientSession from "@/utils/useGetClientSession";
import RealtimeRecords from "@/components/records/realtime-records";
import AddRecordViaRouteHandler from "@/components/records/add-record-via-route-handler";

export default function ClientRouteHandlerFetch() {
  const pathname = usePathname();
  const session = useGetClientSession(pathname);

  const [records, setRecords] = useState<RecordDB[]>([]);

  useEffect(() => {
    (async () => {
      const response = await fetch(
        `${process.env.NEXT_PUBLIC_ORIGIN}/route-handler`
      );

      const data = await response.json();

      if (data) setRecords(data);
    })();
  }, []);

  return (
    session && (
      <>
        <AddRecordViaRouteHandler />
        <RealtimeRecords recordsProp={records} />
      </>
    )
  );
}
