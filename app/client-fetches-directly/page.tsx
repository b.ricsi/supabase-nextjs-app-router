"use client";

import { useEffect, useState } from "react";
import { usePathname, useRouter } from "next/navigation";
import { createClientComponentClient } from "@supabase/auth-helpers-nextjs";
import useGetClientSession from "@/utils/useGetClientSession";
import RealtimeRecords from "@/components/records/realtime-records";
import AddRecordViaRouteHandler from "@/components/records/add-record-via-route-handler";

export default function ClientDirectFetch() {
  const pathname = usePathname();
  const session = useGetClientSession(pathname);

  const [records, setRecords] = useState<RecordDB[]>([]);
  const router = useRouter();

  useEffect(() => {
    (async () => {
      const supabase = createClientComponentClient();

      const { data } = await supabase
        .from("tutorial")
        .select()
        .match({ is_hidden: false });

      if (data) setRecords(data);

      router.refresh();
    })();
  }, [router]);

  return (
    session && (
      <>
        <AddRecordViaRouteHandler />
        <RealtimeRecords recordsProp={records} />
      </>
    )
  );
}
