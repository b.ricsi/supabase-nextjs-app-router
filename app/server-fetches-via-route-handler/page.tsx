// https://jonmeyers.io/blog/forwarding-cookies-from-server-components-to-route-handlers-with-next-js-app-router

import { cookies } from "next/headers";
import { createServerComponentClient } from "@supabase/auth-helpers-nextjs";

import RealtimeRecords from "@/components/records/realtime-records";
import AddRecordViaRouteHandler from "@/components/records/add-record-via-route-handler";

// do not cache this page
export const revalidate = 0;

export default async function ServerRouteHandlerFetch() {
  // https://nextjs.org/docs/app/api-reference/functions/headers#usage-with-data-fetching
  const response = await fetch(
    `${process.env.NEXT_PUBLIC_ORIGIN}/route-handler`
  );

  const records = await response.json();

  const cookieData = cookies();
  const supabase = createServerComponentClient({
    cookies: () => cookieData,
  });

  const {
    data: { session },
  } = await supabase.auth.getSession();

  return (
    session && (
      <>
        <AddRecordViaRouteHandler />
        <RealtimeRecords recordsProp={records} />
      </>
    )
  );
}
