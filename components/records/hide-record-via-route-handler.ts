export default async function hideRecordViaRouteHandler(id: string) {
  await fetch(`${process.env.NEXT_PUBLIC_ORIGIN}/route-handler`, {
    method: "put",
    body: JSON.stringify({ id }),
  });
}
