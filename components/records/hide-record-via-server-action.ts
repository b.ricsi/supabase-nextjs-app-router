import { createServerActionClient } from "@supabase/auth-helpers-nextjs";
import { cookies } from "next/headers";

export default async function hideRecordViaServerAction(id: string) {
  const cookieData = cookies();

  const supabase = createServerActionClient({
    cookies: () => cookieData,
  });

  await supabase.from("tutorial").update({ is_hidden: true }).match({ id });
}
