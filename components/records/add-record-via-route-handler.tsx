"use client";

import RecordInput from "./record-input";

export default function AddRecordViaRouteHandler() {
  const addRecord = async (formData: FormData) => {
    const title = String(formData.get("title"));

    if (title)
      await fetch(`${process.env.NEXT_PUBLIC_ORIGIN}/route-handler`, {
        method: "post",
        body: JSON.stringify({ title }),
      });
  };

  return <RecordInput onFormAction={addRecord} />;
}
