"use client";

import { useEffect, useState } from "react";
import { useRouter } from "next/navigation";
import { createClientComponentClient } from "@supabase/auth-helpers-nextjs";
import Record from "@/components/records/record";
import hideRecordViaRouteHandler from "./hide-record-via-route-handler";

// realtime subscriptions need to be set up client-side
// this component takes initial records as props and automatically
// updates when new records are inserted into Supabase's `records` table
export default function RealtimeRecords({
  recordsProp = [],
}: {
  recordsProp: RecordDB[];
}) {
  const [records, setRecords] = useState(recordsProp);
  const router = useRouter();

  useEffect(() => setRecords(recordsProp), [recordsProp]);

  useEffect(() => {
    // ensure you have enabled replication on the corresponding supabase table
    // https://app.supabase.com/project/_/database/replication

    const supabase = createClientComponentClient();

    const channel = supabase
      .channel("*")
      .on(
        "postgres_changes",
        // https://supabase.com/docs/reference/javascript/subscribe
        {
          event: "*",
          schema: "public",
          table: "tutorial",
        },

        // https://github.com/supabase/supabase/blob/master/examples/auth/nextjs/app/realtime-posts.tsx
        (payload) => {
          const { id: oldId } = payload.old as { id?: string };
          const untouchedRecords = records.filter(({ id }) => id !== oldId);

          setRecords([...untouchedRecords, payload.new as RecordDB]);

          router.refresh();
        }
      )
      .subscribe();

    return () => {
      supabase.removeChannel(channel);
    };
  }, [records, recordsProp, router]);

  return (
    <ul>
      {records.map((record: RecordDB) => (
        <Record
          key={record.id}
          markComplete={hideRecordViaRouteHandler}
          record={record}
        />
      ))}
    </ul>
  );
}
