import { createServerActionClient } from "@supabase/auth-helpers-nextjs";
import { cookies } from "next/headers";
import RecordInput from "./record-input";

export default async function AddRecordViaServerAction() {
  const addRecord = async (formData: FormData) => {
    "use server";
    const title = String(formData.get("title"));

    const cookieData = cookies();
    const supabase = createServerActionClient({
      cookies: () => cookieData,
    });

    if (title) {
      await supabase.from("tutorial").insert({ title });
    }
  };

  return <RecordInput onFormAction={addRecord} />;
}
