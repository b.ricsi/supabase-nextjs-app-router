"use client";

import { useEffect, useState } from "react";
import { useRouter } from "next/navigation";
import { createClientComponentClient } from "@supabase/auth-helpers-nextjs";
import Record from "@/components/records/record";
import hideRecordViaRouteHandler from "./hide-record-via-route-handler";

// realtime subscriptions need to be set up client-side
// this component takes initial posts as props and automatically
// updates when new posts are inserted into Supabase's `posts` table
export default function RealtimeRecord({
  recordProp,
}: {
  recordProp: RecordDB;
}) {
  const [record, setRecord] = useState(recordProp);
  const router = useRouter();

  useEffect(() => {
    setRecord(recordProp);
  }, [recordProp]);

  useEffect(() => {
    // ensure you have enabled replication on the `posts` table
    // https://app.supabase.com/project/_/database/replication

    const supabase = createClientComponentClient();

    const channel = supabase
      .channel("*")
      .on(
        "postgres_changes",
        // https://supabase.com/docs/reference/javascript/subscribe
        {
          event: "UPDATE",
          schema: "public",
          table: "tutorial",
          // https://supabase.com/docs/guides/realtime/postgres-changes#available-filters
          filter: `id=eq.${record?.id}`,
        },

        (payload) => {
          const newRecord = payload.new as RecordDB;

          setRecord(newRecord);

          router.refresh();
        }
      )
      .subscribe();

    return () => {
      supabase.removeChannel(channel);
    };
  }, [record?.id, router]);

  return <Record markComplete={hideRecordViaRouteHandler} record={record} />;
}
