"use client";

import { useState } from "react";

export default function RecordInput({
  onFormAction,
}: {
  onFormAction: (formData: FormData) => Promise<void>;
}) {
  const [title, setTitle] = useState("");

  return (
    <form action={onFormAction} onSubmit={() => setTitle("")}>
      <input
        name="title"
        value={title}
        placeholder="Add todo..."
        onInput={(e) => setTitle(e.currentTarget.value)}
        className="bg-gray-800"
      />
    </form>
  );
}
