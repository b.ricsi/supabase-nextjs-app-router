"use client";

import { useRouter } from "next/navigation";

export default function Record({
  record,
  markComplete,
}: {
  record: RecordDB;
  markComplete: Function;
}) {
  const router = useRouter();

  return (
    !record.is_hidden && (
      <li
        onClick={() => {
          markComplete(record.id);
          router.refresh();
        }}
        className="cursor-pointer"
      >
        {record.title}
      </li>
    )
  );
}
