"use client";

import { useEffect, useState } from "react";
import { usePathname } from "next/navigation";
import { createClientComponentClient } from "@supabase/auth-helpers-nextjs";
import type { Session } from "@supabase/supabase-js";
import Login from "@/components/header/login";
import Navigation from "@/components/header/navigation";

export default function Header() {
  const supabase = createClientComponentClient();
  const pathname = usePathname();
  const [session, setSession] = useState<Session | null>(null);

  useEffect(() => {
    (async () => {
      const {
        data: { session: _session },
      }: { data: { session: Session | null } } =
        await supabase.auth.getSession();

      setSession(_session);
    })();
  }, [supabase.auth, pathname]);

  return (
    <header>
      <Login session={session} />
      <Navigation session={session} />
    </header>
  );
}
