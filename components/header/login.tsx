"use client";

import { createClientComponentClient } from "@supabase/auth-helpers-nextjs";
import type { Session } from "@supabase/supabase-js";
import { useRouter } from "next/navigation";

export default function Login({ session }: { session: Session | null }) {
  const email = process.env.NEXT_PUBLIC_SUPABASE_EMAIL!;
  const password = process.env.NEXT_PUBLIC_SUPABASE_PASSWORD!;

  const supabase = createClientComponentClient();
  const router = useRouter();

  // on sign up, remember to validate the email received

  return (
    <section>
      <nav className="flex gap-2 my-3 justify-center">
        <p>
          {session
            ? `Hello, ${session.user.email}`
            : "Please sign in to see records"}
        </p>

        {/* <button
          className="bg-gray-700 px-1"
          onClick={async () => {
            supabase.auth.signUp({
              email,
              password,
            });
          }}
        >
          Sign up
        </button> */}

        <button
          className="bg-gray-700 px-1"
          onClick={async () => {
            const { error } = await supabase.auth.signInWithPassword({
              email,
              password,
            });

            if (!error) router.push("/client-fetches-directly");
          }}
        >
          Sign in
        </button>

        <button
          className="bg-gray-700 px-1"
          onClick={async () => {
            await supabase.auth.signOut();
            router.push("/unauthenticated");
          }}
        >
          Sign out
        </button>
      </nav>
    </section>
  );
}
