"use client";

import { useEffect, useState } from "react";
import { createClientComponentClient } from "@supabase/auth-helpers-nextjs";
import type { Session } from "@supabase/supabase-js";

export default function useGetClientSession(pathname: string) {
  const supabase = createClientComponentClient();
  const [session, setSession] = useState<Session | null>(null);

  useEffect(() => {
    (async () => {
      const {
        data: { session: _session },
      }: { data: { session: Session | null } } =
        await supabase.auth.getSession();

      setSession(_session);
    })();
  }, [supabase.auth, pathname]);

  return session;
}
