import type { Database as DB } from "@/utils/supabase-types";

declare global {
  type Database = DB;
  type RecordDB = DB["public"]["Tables"]["tutorial"]["Row"];
}
